package char5;

public class chapter01 {
    public void m(int x){
        System.out.println(x%10);
        System.out.println("---------------------");
        if ((x/10)!=0){
            m(x/10);
            System.out.println("++++++++++++++++++++++");
        }
        System.out.println("**********************");
        System.out.print(x%10);
    }
    public static void main(String[] args) {
        chapter01 c=new chapter01();
        c.m(1234);
    }
}