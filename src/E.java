
public class E {
	public static void main(String[] args) {
	
		System.out.println("byte的取值范围："+Byte.MIN_VALUE+"至"+Byte.MAX_VALUE);
		System.out.println("short的取值范围："+Short.MIN_VALUE+"至"+Short.MAX_VALUE);
		System.out.println("int的取值范围："+Integer.MIN_VALUE+"至"+Integer.MAX_VALUE);
		System.out.println("long的取值范围："+Long.MIN_VALUE+"至"+Long.MAX_VALUE);
		System.out.println("float的取值范围："+Float.MIN_VALUE+"至"+Float.MAX_VALUE);
		System.out.println("double的取值范围："+Double.MIN_VALUE+"至"+Double.MAX_VALUE);
		
		long[] a = { 1, 2, 3, 4};
		long[] b = { 100,200,300,400,500};
		b = a;
		System.out.println("数组b的长度："+b.length);
		System.out.println("b[0] = "+b[0]);
}
}
