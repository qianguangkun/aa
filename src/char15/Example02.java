package char15;

import java.util.LinkedList;

public class Example02 {
    public static void main(String[] args) {
        LinkedList link = new LinkedList(); //创建LinkedList集合，初始为空列表
        link.add("stu1");
        link.add("stu2");
        link.add("stu3");
        link.add("stu4");
        System.out.println(link.toString()); //打印集合中的元素
        link.add(3,"Student"); //向集合指定位置插入元素
        link.addFirst("First");//向集合第一个位置插入元素
        System.out.println(link); //另一种方式打印集合中的元素
        System.out.println(link.getFirst());//打印第一个元素
        link.remove(3); //删除索引值为3的元素
        link.removeFirst();//删除第一个元素
        System.out.println(link);

    }

}
