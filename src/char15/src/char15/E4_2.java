package char15.src.char15;

class Point {
    int x, y;
    Point(int a, int b) {
        x = a;
        y = b;
        //System.out.println("（"+x+", "+y+")");
    }
}

public class E4_2 {
    public static void main(String[] args) {
      Point p1,p2; //声明变量0
      p1 = new Point(10,10);//使用new运算符 和 类的构造方法 为声明的对象 分配变量
      p2 = new Point(23,35);
        System.out.println(" p1.x"+p1.x+ "p.y"+p1.y);
      System.out.println("P1:"+p1+"  p2:"+p2);
    }
}
