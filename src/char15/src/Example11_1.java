package char15.src;

abstract class MotorVehicles {
  abstract void brake();
}

interface MoneyFare{
    void charge();
        }

interface ControlTemparature{
    void controlAirTemparature();
        }

class Bus extends MotorVehicles implements MoneyFare {

 void brake(){
   System.out.println("公共汽车使用够使刹车技术");
 }

public  void charge(){
   System.out.println("一元一张，不计公里数");
 }

}

class Taxi extends MotorVehicles implements MoneyFare, ControlTemparature {

  @Override
  void brake() {
    System.out.println("出租车使用盘 刹车技术");
  }
  public void charge() {
    System.out.println("2元一公里");
  }
  public void controlAirTemparature(){
    System.out.println("出租车安装了空调");
  }
}

class Cinema implements MoneyFare, ControlTemparature {
 public void charge(){
   System.out.println("电影院十元一张");
 }
@Override
public void controlAirTemparature() {
   // TODO Auto-generated method stub
   System.out.println("安装了中央空调");
}
}


public class Example11_1 {
	public  static  void  main(String  args[]) {

	    Bus bus101 =new Bus();
	    Taxi blueTaxi =new Taxi();
	    Cinema redStarCinema = new Cinema();
	    bus101.brake();
	    bus101.charge();
	    blueTaxi.brake();
	    blueTaxi.charge();
	    blueTaxi.controlAirTemparature();
	    redStarCinema.charge();
	    redStarCinema.controlAirTemparature();
	    System.out.println("helel");
	  }
}
