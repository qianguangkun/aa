package char15;

import java.util.ArrayList;
import java.util.Iterator;

public class Example06 {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add("Jack");
        list.add("Annie");
        list.add("Rose");
        list.add("Tom");
        Iterator it = list.iterator(); //获得Iterator对象
        while(it.hasNext()){  //判断该集合是否有下一个元素
            Object obj = it.next(); //获取该集合中的元素
            /*if("Annie".equals(obj)){ //判断该集合中的元素是否为Annie
                list.remove(obj);  //删除该集合中的元素
                //会抛出异常  增删集合中的元素会导致预期迭代次数无效
                // 解决方法一 删除某个元素后跳出循环不再迭代
                break;
            }*/
            //解决方法二
            if("Annie".equals(obj)) {
                it.remove();
            }
        }
        System.out.println(list);
    }
}
