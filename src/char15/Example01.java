package char15;


import java.util.ArrayList;

public class Example01 {
    public static void main(String[] args) {
        ArrayList list = new ArrayList(); //创建集合，初始容量缺省为10
        ArrayList list1 = new ArrayList(5);// 创建集合，初始容量缺省为5
        list.add("stu1");
        list.add("stu2");
        list.add("stu3");
        list.add("stu4");
        System.out.println("集合的长度："+list.size()); //获取集合中元素个数
        System.out.println("第3个元素是："+list.get(2));
    }
}
