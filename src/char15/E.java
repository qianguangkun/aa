package char15;

import java.io.PrintStream;

public class E {
    public static void main(String[] args) {

        //
        float price = 12.5f;
        System.out.println("float类型"+price);

        //byte   age = 128;
        byte   age = 127;
        System.out.println("byte的类型"+age);

        System.out.println("byte取值范围:"+ Byte.MIN_VALUE+"至"+Byte.MAX_VALUE);
        System.out.println("short取值范围:"+ Short.MIN_VALUE+"至"+Short.MAX_VALUE);
        System.out.println("int取值范围:"+ Integer.MIN_VALUE+"至"+Integer.MAX_VALUE);
        System.out.println("long取值范围:"+ Long.MIN_VALUE+"至"+Long.MAX_VALUE);
        System.out.println("float取值范围:"+ Float.MIN_VALUE+"至"+Float.MAX_VALUE);
        System.out.println("double取值范围:"+ Double.MIN_VALUE+"至"+Double.MAX_VALUE);

    }
}
