package char15;

import java.util.ArrayList;
import java.util.Iterator;

public class Example03 {
    public static void main(String[] args) {
        ArrayList  list = new ArrayList(); //创建ArrayList集合
        list.add("data1");
        list.add("data2");
        list.add("data3");
        list.add("data4");
        list.add("data5");
        Iterator it = list.iterator(); //获取Iterator 迭代器
        while(it.hasNext()) { // 判断是否存在下一个元素
           Object obj = it.next();
            System.out.println(obj);

        }
    }
}
